`focus-settings` is a `urxvt` extension that changes settings dynamically when the window gains or loses focus. It is based on the extension posted as a comment at https://github.com/jwilm/alacritty/issues/1982. The supported settings are:

- foreground color
- background color

To enable the extension, first copy it to `~/.urxvt/ext`. Then add `focus-settings` to the `URxvt.perl-ext` or `URxvt.perl-ext-common` resource in your `~/.Xresources` file. For example:

```
URxvt.perl-ext-common: default,confirm-paste,focus-settings
```

To configure the extension, add lines like any of the following to your `~/.Xresources` file:

```
URxvt.focus-settings.foreground-focused: #000000
URxvt.focus-settings.background-focused: #cccccc
URxvt.focus-settings.foreground-unfocused: #444444
URxvt.focus-settings.background-unfocused: #aaaaaa
```

If any of the focused/unfocused settings provided by this extension are omitted from `~/.Xresources`, they will default to the value of the corresponding standard resource setting (`URxvt.foreground` and `URxvt.background`, in this case).
